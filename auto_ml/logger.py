import logging
import sys


def init_logger():
    root_logger = logging.getLogger()
    root_logger.setLevel(logging.INFO)

    console_handler = logging.StreamHandler(sys.stdout)
    root_logger.addHandler(console_handler)
