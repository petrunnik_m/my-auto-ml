from typing import List

import numpy as np
import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import OneHotEncoder

IMPUTER_STRATEGY_NUM = 'mean'
IMPUTER_STRATEGY_CAT = 'most_frequent'


def get_imputer(is_num=True) -> SimpleImputer:
    strategy = IMPUTER_STRATEGY_NUM if is_num else IMPUTER_STRATEGY_CAT
    imputer = SimpleImputer(missing_values=np.nan, strategy=strategy)
    return imputer


def process_cat_features(pipeline: Pipeline, cat_data: pd.DataFrame) -> pd.DataFrame:
    one_data = pd.DataFrame(
        pipeline.transform(cat_data).toarray(),
        columns=pipeline["encoder"].get_feature_names(),
    )
    return one_data


def cat_pipeline() -> Pipeline:
    imputer = get_imputer(is_num=False)
    encoder = OneHotEncoder()
    pipeline = Pipeline([
        ("imputer", imputer),
        ("encoder", encoder)
    ])
    return pipeline


def num_pipeline() -> Pipeline:
    imputer = get_imputer()
    pipeline = Pipeline([
        ("imputer", imputer),
        ("normalizer", Normalizer(norm='l1'))
    ])
    return pipeline


def process_X(transformer: ColumnTransformer, X: pd.DataFrame) -> pd.DataFrame:
    X = transformer.transform(X)
    if isinstance(X, np.ndarray):
        return X
    else:
        return pd.DataFrame(X.toarray())


def column_transformer(cat_features: List[str], num_features: List[str]) -> ColumnTransformer:
    transformer = ColumnTransformer(
        [
            ("categorical", cat_pipeline(), cat_features),
            ("numerical", num_pipeline(), num_features),
        ]
    )
    return transformer
