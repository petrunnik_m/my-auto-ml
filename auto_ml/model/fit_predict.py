import concurrent.futures
import logging
from typing import Union, Tuple, List, Dict

import numpy as np
import pandas as pd
from catboost import CatBoostClassifier, CatBoostRegressor
from lightgbm import LGBMRegressor, LGBMClassifier
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression, LogisticRegression
from sklearn.model_selection import cross_validate

RegressionModel = Union[RandomForestRegressor, LinearRegression, CatBoostRegressor, LGBMRegressor]
ClassificationModel = Union[RandomForestClassifier, LogisticRegression, CatBoostClassifier, LGBMClassifier]
Model = Union[RegressionModel, ClassificationModel]


def get_model_metric(
        model_param: List,
        X: pd.DataFrame,
        y: pd.Series,
        scoring: str,
        k_folds: int,
        logger: logging.Logger = logging.getLogger(__name__)
) -> Dict:
    '''
    Here the program gets metrics of training model.
    It gets model, mean score and execution time for k_folds (number of folds)
    '''
    logger.info(f"Getting metrics for {model_param[0].__name__} model...")
    model = model_param[0](**model_param[1])
    model_score = cross_validate(model, X, y, cv=k_folds, scoring=scoring)
    model_metric = {
        'model': model,
        'model_name': model_param[0].__name__,
        'score_name': scoring,
        'score': np.mean(model_score['test_score']),
        'time': np.mean(model_score['fit_time'])}
    return model_metric


def train_model(
        X: pd.DataFrame,
        y: pd.Series,
        model_params: List[List],
        max_process: int,
        scoring: str,
        k_folds: int,
        logger: logging.Logger = logging.getLogger(__name__)
) -> Tuple[Model, pd.DataFrame]:
    model_metric = pd.DataFrame(columns=['model', 'model_name', 'score_name', 'score', 'time'])

    with concurrent.futures.ProcessPoolExecutor(max_workers=max_process) as executor:
        futures = [executor.submit(get_model_metric, model_param, X, y, scoring, k_folds, logger) for model_param in model_params]
        for future in concurrent.futures.as_completed(futures):
            model_metric = model_metric.append(future.result(), ignore_index=True)

    model_metric = model_metric.sort_values(by='score', ascending=False)
    model_metric['score'] = model_metric['score'].round(3)
    best_model = model_metric.iloc[0]
    model_metric.drop(columns=['model'])
    logger.info(f"We have chosen {best_model['model']} model "
                f"with {best_model['score_name']} = {best_model['score']}."
                f"Training with it.")
    best_model = best_model['model']
    best_model.fit(X, y)

    return best_model, model_metric


def predict_model(model: Model, features: pd.DataFrame) -> np.ndarray:
    predicts = model.predict(features)
    return predicts


def save_predicts(predicts: np.ndarray, path_to_predicts: str) -> None:
    pd.DataFrame(predicts).to_csv(path_to_predicts)
