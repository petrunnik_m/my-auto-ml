class BaseAutoMLException(Exception):
    pass


class MethodOrderIncorrectException(BaseAutoMLException):
    pass
