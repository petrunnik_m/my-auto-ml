from typing import Tuple, Optional

import pandas as pd


def extract_X_y(dataset: pd.DataFrame, y_name: str) -> Tuple[pd.DataFrame, Optional[pd.Series]]:
    X = dataset.drop(columns=[y_name])
    y = dataset[y_name]
    return X, y
