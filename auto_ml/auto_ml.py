import logging
import pickle
from typing import List, Optional, Tuple
import abc

import numpy as np
import pandas as pd
from catboost import CatBoostRegressor, CatBoostClassifier
from lightgbm import LGBMRegressor, LGBMClassifier
from sklearn.compose import ColumnTransformer
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression, LogisticRegression

from auto_ml.data.make_dataset import extract_X_y
from auto_ml.exception import MethodOrderIncorrectException
from auto_ml.logger import init_logger
from auto_ml.model.fit_predict import train_model, predict_model, save_predicts, Model
from auto_ml.preprocessing.build_features import column_transformer, process_X

__all__ = ["AutoMLRegressor", "AutoMLClassifier"]

init_logger()
logger = logging.getLogger(__name__)


class _BaseAutoML(abc.ABC):

    def __init__(self, n_process: int = 3) -> None:
        self.n_process: int = n_process
        self._feature_transformer: Optional[ColumnTransformer] = None
        self.best_model: Optional[Model] = None

    @property
    @abc.abstractmethod
    def model_params(self) -> List[List]:
        pass

    def preprocess_fit_transform(
            self,
            dataset: pd.DataFrame,
            cat_features: List[str],
            num_features: List[str],
            y_name: str,
    ) -> Tuple[pd.DataFrame, pd.Series]:
        X_train, y_train = extract_X_y(dataset, y_name)
        self._feature_transformer = column_transformer(cat_features, num_features)
        self._feature_transformer.fit(X_train)

        X_train = process_X(
            self._feature_transformer,
            X=X_train
        )

        logger.info(f"train_features.shape is {X_train.shape}")
        return X_train, y_train

    def preprocess_transform(self, dataset: pd.DataFrame) -> pd.DataFrame:
        if self._feature_transformer is None:
            raise MethodOrderIncorrectException("You haven't fit the transformer yet. "
                                                "You should use preprocess_fit_transform() before this action.")
        features = process_X(
            self._feature_transformer,
            X=dataset
        )
        logger.info(f"features.shape for test dataset is {features.shape}")
        return features

    def fit(self, X: pd.DataFrame, y: pd.Series, scoring: str, k_folds: int = 5) -> pd.DataFrame:
        self.best_model, model_metric = train_model(X, y, self.model_params, self.n_process, scoring, k_folds, logger)
        return model_metric

    def predict(self, X: pd.DataFrame) -> np.ndarray:
        if self.best_model is None:
            raise MethodOrderIncorrectException("You haven't trained the model yet."
                                                "You should use fit() before this action.")
        predicts = predict_model(
            self.best_model, X
        )
        return predicts

    def save_model(self, path: str) -> None:
        with open(path, "wb") as f:
            pickle.dump(self.best_model, f)
        logger.info(f"The best model has been saved here: {path}")

    def full_pipeline(
            self,
            dataset: pd.DataFrame,
            cat_features: List[str],
            num_features: List[str],
            y_name: str,
            scoring,
            k_folds=5,
            predict_dataset: Optional[pd.DataFrame] = None,
            path_to_model: str = None,
            path_to_predicts: str = None
    ) -> Tuple[Optional[np.ndarray], pd.DataFrame]:
        logger.info(f"Start full training pipeline")

        X_train, y_train = self.preprocess_fit_transform(dataset, cat_features, num_features, y_name)

        model_metric = self.fit(X_train, y_train, scoring=scoring, k_folds=k_folds)
        predicts = None
        if predict_dataset is not None:
            pred_features = self.preprocess_transform(predict_dataset)
            predicts = self.predict(pred_features)
            if path_to_predicts:
                save_predicts(predicts, path_to_predicts)
                logger.info(f"Predictions by the best model have been saved here: {path_to_predicts}")

        if path_to_model:
            self.save_model(path_to_model)

        return predicts, model_metric


class AutoMLRegressor(_BaseAutoML):
    def __init__(self, n_process: int = 5) -> None:
        super().__init__(n_process)

    @property
    def model_params(self) -> List[List]:
        return [
            [RandomForestRegressor, {'n_estimators': 30}],
            [LinearRegression, {}],
            [CatBoostRegressor, {'logging_level': 'Silent'}],
            [LGBMRegressor, {}]
        ]


class AutoMLClassifier(_BaseAutoML):
    def __init__(self, n_process: int = 5) -> None:
        super().__init__(n_process)

    @property
    def model_params(self) -> List[List]:
        return [
            [RandomForestClassifier, {'n_estimators': 30}],
            [LogisticRegression, {'max_iter': 500}],
            [CatBoostClassifier, {'logging_level': 'Silent'}],
            [LGBMClassifier, {}]
        ]
