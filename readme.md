my-auto-ml
-----------

Библиотека для автоматического выбора наилучшей модели для датасета с учетом указанной метрики.

# Содержание

- [Установка](#Установка)
- [Разработка](#Разработка)
    - [Настройка окружения](#Настройка-окружения)
    - [Генерация setup.py](#Генерация-setup.py)
- [API библиотеки](#API-библиотеки)
- [Примеры использования](#Примеры-использования)
- [Планы на будущее](#Планы-на-будущее)



<a name='Установка'></a>
## Установка

Установить библиотеку можно 2 способами:

- pip

  ```
  pip install git+https://gitlab.com/petrunnik_m/my-auto-ml.git
  ```

- poetry

  ```
  poetry add git+https://gitlab.com/petrunnik_m/my-auto-ml.git
  ```


<a name='Разработка'></a>
## Разработка

<a name='Настройка-окружения'></a>
### Настройка окружения

**Зависимости**

- python 3.8
- poetry >= 1.1.6


- клониурем проект;

- создаем виртуальное окружение для проекта;

- устанавливаем билиотеки 

    ```
    poetry install
    ```

<a name='Генерация-setup.py'></a>
### Генерация setup.py

После каждого обновления pyproject.toml необходимо обновлять файл setup.py, чтобы была возможность установить библиотеку
из git репозитория и проекты не использующие poetry могли использовать ее.

Сделать это можно при помощи команды:

```
make build_setuppy
```


<a name='API-библиотеки'></a>
## API библиотеки

**class AutoMLRegressor(n_process=5)**

Библиотека для автоматического выбора наилучшей модели для датасета с учетом указанной метрики 

| **Parameters**      |n_process: int, default=True - Максимальное колличество процессов, в которых модели будут обучаться отдельно |
| ----------- | ----------- |
| **Attributes** | **best_model: RandomForestRegressor or LinearRegression or CatBoostRegressor or LGBMRegressor**

**Методы**

| Название метода     | Описание |
| ----------- | ----------- |
| preprocess_fit_transform(dataset, cat_features, num_features, y_name)| Предобработать тренировочные данные |
| preprocess_transform(dataset)   | Предобработать тестовые данные данные |
|fit(X, y, scoring[, k_folds])| Получить метрики от всех моделей, обучить наилучшую модель|
|predict(X)| Получить предсказания с помощью наилучшей модели|
|save_model(path)| Сохранить наилучшую модель|
|full_pipeline(dataset, cat_features, num_features, y_name, scoring[, k_folds, predict_dataset, path_to_model, path_to_predicts) | Запустить полный процесс от предобработки до сохранения модели

**_full_pipeline_**

| Parameters      | Description |
| ----------- | ----------- |
| dataset      | DataFrame - данные для обучения|
| cat_features   | List[str] - список названий категориальных данных|
|num_features|List[str] - список названий числовых данных|
|y_name|str - имя целевого столбца|
|scoring|str - метрика качества, с помощью которой сортируем алгоритмы|
|k_folds|int, default=5|
|predict_dataset|DataFrame - данные для предсказания|
|path_to_model|str - путь, по которому будет сохранена наилучшая модель|
|path_to_predicts|str - путь, по которому будут сохранены предсказания|
            
Возвращает -> predicts(np.ndarray), model_metric(pd.DataFrame) - предсказания на тестовых данных, метрики всех моделей

**class AutoMLClassifier(n_process=5)**

Библиотека для автоматического выбора наилучшей модели для датасета с учетом указанной метрики 

| **Parameters**      |n_process: int, default=True - Максимальное колличество процессов, в которых модели будут обучаться отдельно |
| ----------- | ----------- |
| **Attributes** | **best_model: RandomForestClassifier, LogisticRegression, CatBoostClassifier, LGBMClassifier**

**Методы**

| Название метода     | Описание |
| ----------- | ----------- |
| preprocess_fit_transform(dataset, cat_features, num_features, y_name)| Предобработать тренировочные данные |
| preprocess_transform(dataset)   | Предобработать тестовые данные данные |
|fit(X, y, scoring[, k_folds])| Получить метрики от всех моделей, обучить наилучшую модель|
|predict(X)| Получить предсказания с помощью наилучшей модели|
|save_model(path)| Сохранить наилучшую модель|
|full_pipeline(dataset, cat_features, num_features, y_name, scoring[, k_folds, predict_dataset, path_to_model, path_to_predicts) | Запустить полный процесс от предобработки до сохранения модели

**_full_pipeline_**

| Parameters      | Description |
| ----------- | ----------- |
| dataset      | DataFrame - данные для обучения|
| cat_features   | List[str] - список названий категориальных данных|
|num_features|List[str] - список названий числовых данных|
|y_name|str - имя целевого столбца|
|scoring|str - метрика качества, с помощью которой сортируем алгоритмы|
|k_folds|int, default=5|
|predict_dataset|DataFrame - данные для предсказания|
|path_to_model|str - путь, по которому будет сохранена наилучшая модель|
|path_to_predicts|str - путь, по которому будут сохранены предсказания|
            
Возвращает -> predicts(np.ndarray), model_metric(pd.DataFrame) - предсказания на тестовых данных, метрики всех моделей


<a name='Примеры-использования'></a>
## Примеры использования

Пример использования библиотеки для задачи регрессии:
`examples/example_reg.ipynb`

Пример использования библиотеки для задачи классификации:
`examples/example_class.ipynb`


<a name='Планы-на-будущее'></a>
## Планы на будущее

- Добавить другие алгоритмы
- Добавить возможность выбирать лучший алгоритм не только по метрике качества,
но и по времени работы алгоритма
- Добавить проверку признаков на информативность, сделать возможным сокращение количества признаков
- Добавить поиск лучших гиперпараметров для алгоритмов с помощью GridSearchCv
- Добавить автотесты
- Добавить возможность генерировать графики для визуализации метрик проверенных алгоритмов
